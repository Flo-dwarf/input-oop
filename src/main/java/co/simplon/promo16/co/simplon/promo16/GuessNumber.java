package co.simplon.promo16.co.simplon.promo16;

import java.util.InputMismatchException;
import java.util.Scanner;

public class GuessNumber {

    private int answer = 5;
    private boolean over = false;

    public String guess(int input) {
        if (answer < input) {
            return "it's lesser than that";
        }
        if (answer > input) {
            return "it's greater than that";
        }
        over = true;
        return "Congratz";
    }

    public void run() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Guess number : ");
        while (!over) {
            try {
                int input = scanner.nextInt();
                String feedback = guess(input);
                System.out.println(feedback);
            } catch (InputMismatchException e) {
                System.out.println("Only numbers accepted");
                scanner.next();
            }
        }
        scanner.close();
    }

    public void setAnswer(int answer) {
        this.answer = answer;
    }

    public boolean isOver() {
        return over;
    }

}
