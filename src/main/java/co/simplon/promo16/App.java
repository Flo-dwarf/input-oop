package co.simplon.promo16;

import co.simplon.promo16.co.simplon.promo16.GuessNumber;

public class App {
    public static void main(String[] args) {
        GuessNumber guessNumber = new GuessNumber();
        guessNumber.run();
    }
}
