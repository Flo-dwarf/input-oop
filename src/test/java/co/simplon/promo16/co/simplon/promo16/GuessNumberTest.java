package co.simplon.promo16.co.simplon.promo16;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class GuessNumberTest {
    GuessNumber guesser;

    @Before
    public void init(){
        guesser = new GuessNumber();
        guesser.setAnswer(5);
    }

    @Test
    public void shouldSayGreater() {
        assertEquals("it's greater than that", guesser.guess(2));
        assertFalse(guesser.isOver());
    }
    @Test
    public void shouldSayLesser() {
        assertEquals("it's lesser than that", guesser.guess(6));
        assertFalse(guesser.isOver());

    }
    @Test
    public void shouldSayCongratz() {
        assertEquals("Congratz", guesser.guess(5));
        assertTrue(guesser.isOver());

    }
}
